.PHONY: build

build:
	go build .

run:
	./vedmachestvo

start:
	go build .
	./vedmachestvo
test:
	docker-compose -f tests/docker-compose.yml up --build -d
	go test -v ./tests
	docker-compose -f tests/docker-compose.yml down


.DEFAULT_GOAL := start