package models

import "gorm.io/gorm"

type Law struct {
	gorm.Model
	Name        string `json:"name"`
	Location    string `json:"location"`
	Description string `json:"description"`
	WitcherID   uint   `json:"witcher_id" gorm:"constraint:OnDelete:CASCADE;"`
	Witcher     User   `json:"witcher"`
}
