package models

import "gorm.io/gorm"

type User struct {
	gorm.Model
	Email         string  `gorm:"unique" json:"email"`
	Password      string  `json:"-"`
	Name          string  `json:"name"`
	Description   string  `json:"description"`
	Role          string  `json:"role"`
	Level         int     `json:"level"`
	Agility       int     `json:"agility"`
	Intelligence  int     `json:"intelligence"`
	Strength      int     `json:"strength"`
	IsDead        bool    `json:"is_dead"`
	IsBanned      bool    `json:"is_banned"`
	WitcherID     *uint   `json:"witcher_id"`
	Witcher       *User   `json:"witcher"`
	Students      []User  `gorm:"foreignkey:WitcherID" json:"-"`
	StudentQuests []Quest `gorm:"foreignkey:StudentID" json:"-"`
	WitcherQuests []Quest `gorm:"foreignkey:WitcherID" json:"-"`
	WitcherLaws   []Law   `gorm:"foreignkey:WitcherID" json:"-"`
}
