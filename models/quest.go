package models

import (
	"gorm.io/gorm"
)

type Quest struct {
	gorm.Model
	Title               string `json:"title"`
	Description         string `json:"description"`
	PreferredLevel      int    `json:"preferred_level"`
	MostNeededAttribute string `json:"most_needed_attribute"`
	Status              string `json:"status"`
	IsDone              bool   `json:"is_done"`
	WitcherID           *uint  `json:"witcher_id"`
	Witcher             *User  `json:"witcher"`
	StudentID           *uint  `json:"student_id"`
	Student             *User  `json:"student"`
}

const (
	New        = "New"
	ToDo       = "ToDo"
	InProgress = "InProgress"
	InReview   = "InReview"
	Done       = "Done"
)
