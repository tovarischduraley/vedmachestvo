package middleware

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"vedmachestvo/helpers"
	"vedmachestvo/models"
)

func RequireRoleTeacher(ctx *gin.Context) {

	user, _ := ctx.Get("user")

	if user.(models.User).Role != helpers.Witcher && user.(models.User).Role != helpers.Headmaster {
		ctx.AbortWithStatus(http.StatusForbidden)
	}

	ctx.Next()
}

func RequireRoleDeath(ctx *gin.Context) {

	user, _ := ctx.Get("user")

	if user.(models.User).Role != helpers.Death {
		ctx.AbortWithStatus(http.StatusForbidden)
	}

	ctx.Next()
}
