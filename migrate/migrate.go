package main

import (
	"log"
	"vedmachestvo/initializers"
	"vedmachestvo/models"
)

func init() {
	initializers.LoadEnvVariables()
	initializers.ConnectToDB()
}

func main() {
	err := initializers.DB.AutoMigrate(models.User{}, models.Quest{}, models.Law{})
	if err != nil {
		log.Fatal(err)
	}
}
