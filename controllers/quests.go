package controllers

import (
	"net/http"
	"strconv"
	"vedmachestvo/helpers"
	"vedmachestvo/initializers"
	"vedmachestvo/models"
	"vedmachestvo/services"

	"github.com/gin-gonic/gin"
)

func HandleGetQuests(ctx *gin.Context) {

	var quests []models.Quest
	base := initializers.DB

	status := ctx.Query("status")

	witcherID := ctx.Query("witcher_id")
	if witcherID != "" {
		if witcherID == "null" {
			base = base.Where("witcher_id IS NULL")
		} else {
			WID, err := strconv.Atoi(witcherID)
			if err != nil {
				ctx.AbortWithError(http.StatusBadRequest, err)
				return
			}
			uWID := uint(WID)
			base = base.Where(&models.Quest{WitcherID: &uWID})
		}
	}

	studentID := ctx.Query("student_id")
	if studentID != "" {
		if studentID == "null" {
			base = base.Where("student_id IS NULL")
		} else {
			SID, err := strconv.Atoi(studentID)
			if err != nil {
				ctx.AbortWithError(http.StatusBadRequest, err)
				return
			}
			uSID := uint(SID)
			base = base.Where(&models.Quest{StudentID: &uSID})
		}
	}

	if status != "" {
		base = base.Where(&models.Quest{Status: status})
	}

	ret := base.Preload("Witcher").Preload("Student").Find(&quests)

	if ret.Error != nil {
		ctx.AbortWithError(http.StatusBadRequest, ret.Error)
		return
	}
	ctx.JSON(http.StatusOK, quests)
}

func HandleGetQuestById(ctx *gin.Context) {
	id := ctx.Param("id")

	quest, err := services.GetQuestById(id)
	if err != nil {
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	ctx.JSON(http.StatusOK, quest)
}

func HandleDeleteQuest(ctx *gin.Context) {
	id := ctx.Param("id")

	err := services.DeleteQuest(id)
	if err != nil {
		ctx.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{})
}

func HandleCreateQuest(ctx *gin.Context) {
	var body helpers.InputQuestCreateSchema

	if ctx.Bind(&body) != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Failed to read body",
		})
		return
	}

	var student *uint
	var status string
	if body.StudentID != 0 {
		status = models.ToDo
		student = &body.StudentID
	} else {
		status = models.New
		student = nil
	}

	witcher, _ := ctx.Get("user")
	witcherID := witcher.(models.User).ID
	quest := models.Quest{
		Title:               body.Title,
		Description:         body.Description,
		PreferredLevel:      body.PreferredLevel,
		MostNeededAttribute: body.MostNeededAttribute,
		WitcherID:           &witcherID,
		StudentID:           student,
		Status:              status,
	}

	result := initializers.DB.Create(&quest)

	if result.Error != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Failed to create quest",
		})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{})
}

func HandleUpdateQuest(ctx *gin.Context) {

	var body struct {
		Title               string `json:"title,omitempty"`
		Description         string `json:"description,omitempty"`
		PreferredLevel      int    `json:"preferred_level,omitempty"`
		MostNeededAttribute string `json:"most_needed_attribute,omitempty"`
		Status              string `json:"status,omitempty"`
		IsDone              bool   `json:"is_done,omitempty"`
		WitcherID           uint   `json:"witcher_id,omitempty"`
		StudentID           uint   `json:"student_id,omitempty"`
	}

	if ctx.Bind(&body) != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Failed to read body",
		})
		return
	}

	id := ctx.Param("id")
	ret := initializers.DB.Model(&models.Quest{}).Where("id = ?", id).Updates(body)

	if ret.Error != nil {
		ctx.AbortWithError(http.StatusBadRequest, ret.Error)
		return
	}

	ctx.JSON(http.StatusOK, gin.H{})
}

func HandleCheckQuestIsReady(ctx *gin.Context) {
	id := ctx.Param("id")

	response, err := services.CheckQuestIsReady(id)
	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	ctx.JSON(http.StatusOK, response)
}
