package controllers

import (
	"fmt"
	"net/http"
	"vedmachestvo/exceptions"
	"vedmachestvo/helpers"
	"vedmachestvo/services"

	"github.com/gin-gonic/gin"
)

func HandleGetLaws(ctx *gin.Context) {
	witcherID := ctx.Query("witcher_id")
	laws, err := services.GetLaws(witcherID)
	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}
	ctx.JSON(http.StatusOK, laws)
}

func HandleGetLawById(ctx *gin.Context) {
	id := ctx.Param("id")

	law, err := services.GetLawById(id)
	if err != nil {
		if _, ok := err.(*exceptions.NotFoundError); ok != true {
			ctx.AbortWithStatus(http.StatusNotFound)
			return
		}
		if _, ok := err.(*exceptions.StoreError); ok != true {
			ctx.AbortWithStatus(http.StatusBadRequest)
			return
		}
	}

	ctx.JSON(http.StatusOK, law)
}

func HandleCreateLaw(ctx *gin.Context) {
	var body helpers.InputLawSchema

	if ctx.Bind(&body) != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Failed to read body",
		})
		return
	}
	fmt.Println(body.WitcherID)
	fmt.Println(body.Location)
	err := services.CreateLaw(&body)
	if err != nil {
		println(err.Error())
		if _, ok := err.(*exceptions.ParseError); ok {
			ctx.AbortWithStatus(http.StatusBadRequest)
			return
		}
		if _, ok := err.(*exceptions.StoreError); ok {
			ctx.AbortWithStatus(http.StatusInternalServerError)
			return
		}
	}

	ctx.JSON(http.StatusCreated, gin.H{})
}

func HandleUpdateLaw(ctx *gin.Context) {
	var body helpers.InputLawUpdateSchema

	if ctx.Bind(&body) != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Failed to read body",
		})
		return
	}

	id := ctx.Param("id")
	err := services.UpdateLaw(id, &body)

	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	ctx.JSON(http.StatusOK, gin.H{})
}

func HandleLawUpgrade(ctx *gin.Context) {

	var body helpers.InputLawUpgradeSchema

	if ctx.Bind(&body) != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Failed to read body",
		})
		return
	}
	id := ctx.Param("id")

	err := services.UpgrageLaw(id, &body)

	if err != nil{
		if _, ok := err.(*exceptions.StoreError); ok {
			ctx.AbortWithStatus(http.StatusInternalServerError)
			return
		}
		if _, ok := err.(*exceptions.NotFoundError); ok != true {
			ctx.AbortWithStatus(http.StatusNotFound)
			return
		}
		if _, ok := err.(*exceptions.ValidationError); ok != true {
			ctx.AbortWithStatus(http.StatusBadRequest)
			return
		}
	}

	ctx.JSON(http.StatusOK, gin.H{})
}

func HandleDeleteLaw(ctx *gin.Context) {
	id := ctx.Param("id")
	err := services.DeleteLaw(id)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"error": "Failed to delete law",
		})
		return
	}
	ctx.JSON(http.StatusNoContent, gin.H{})
}
