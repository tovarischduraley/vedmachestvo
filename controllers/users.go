package controllers

import (
	"fmt"
	"net/http"
	"os"
	"time"
	"vedmachestvo/exceptions"
	"vedmachestvo/helpers"
	"vedmachestvo/initializers"
	"vedmachestvo/models"
	"vedmachestvo/services"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v5"
	_ "github.com/golang-jwt/jwt/v5"
	"golang.org/x/crypto/bcrypt"
)

func HandleGetUserById(ctx *gin.Context) {
	id := ctx.Param("id")

	user, err := services.GetUserById(id)
	if err != nil {
		if _, ok := err.(*exceptions.NotFoundError); ok != true {
			ctx.AbortWithStatus(http.StatusNotFound)
			return
		}
		if _, ok := err.(*exceptions.StoreError); ok != true {
			ctx.AbortWithStatus(http.StatusInternalServerError)
			return
		}
	}

	ctx.JSON(http.StatusOK, user)
}

func HandleCreateUser(ctx *gin.Context) {
	var body helpers.InputUserCreateSchema

	if ctx.Bind(&body) != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Failed to read body",
		})
		return
	}

	var witcherID *uint
	u, exists := ctx.Get("user")
	if exists && u.(models.User).Role == helpers.Witcher {
		id := u.(models.User).ID
		witcherID = &id
		fmt.Println(witcherID)
	} else {
		witcherID = nil
	}
	body.WitcherID = witcherID
	services.CreateUser(&body)
	ctx.JSON(http.StatusCreated, gin.H{})
}

func HandleLogin(ctx *gin.Context) {
	var body struct {
		Email    string
		Password string
	}

	if ctx.Bind(&body) != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Failed to reaed body",
		})
		return
	}

	var user models.User
	initializers.DB.First(&user, "is_dead = False and is_banned = False and email = ? ", body.Email)
	if user.ID == 0 {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Invalid Email or Password",
		})
		return
	}

	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(body.Password))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Invalid Email or Password",
		})
		return
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sub": user.ID,
		"exp": time.Now().Add(time.Hour * 24 * 30).Unix(),
	})

	tokenString, err := token.SignedString([]byte(os.Getenv("SECRET")))
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"error": "Failed to create token",
		})
		return
	}

	ctx.SetSameSite(http.SameSiteLaxMode)
	ctx.SetCookie("Authorization", tokenString, 3600*24*30, "/", "localhost", false, true)

	ctx.JSON(http.StatusOK, gin.H{})
}

func HandleLogout(ctx *gin.Context) {
	ctx.SetCookie("Authorization", "", -1, "/", "localhost", false, true)
	ctx.JSON(http.StatusOK, gin.H{})
}

func HandleValidate(ctx *gin.Context) {

	user, _ := ctx.Get("user")

	ctx.JSON(http.StatusOK, user)
}

func HandleKillUser(ctx *gin.Context) {
	id := ctx.Param("id")

	err := services.KillUser(id)
	if err != nil {
		
	}

	ctx.JSON(http.StatusOK, gin.H{})

}

func HandleUpgradeStudent(ctx *gin.Context) {
	id := ctx.Param("id")

	err := services.UpgrageStudent(id)
	if err != nil{
		if _, ok := err.(*exceptions.StoreError); ok {
			ctx.AbortWithStatus(http.StatusInternalServerError)
			return
		}
		if _, ok := err.(*exceptions.NotFoundError); ok != true {
			ctx.AbortWithStatus(http.StatusNotFound)
			return
		}
	}

	ctx.JSON(http.StatusOK, gin.H{})
}
func HandleUnbanUser(ctx *gin.Context) {
	id := ctx.Param("id")
	
	err := services.UnbanUser(id)
	if err != nil{
		if _, ok := err.(*exceptions.StoreError); ok {
			ctx.AbortWithStatus(http.StatusInternalServerError)
			return
		}
		if _, ok := err.(*exceptions.NotFoundError); ok != true {
			ctx.AbortWithStatus(http.StatusNotFound)
			return
		}
	}
	ctx.JSON(http.StatusOK, gin.H{})
}

func HandleBanUser(ctx *gin.Context) {
	id := ctx.Param("id")

	err := services.BanUser(id)
	if err != nil{
		if _, ok := err.(*exceptions.StoreError); ok {
			ctx.AbortWithStatus(http.StatusInternalServerError)
			return
		}
		if _, ok := err.(*exceptions.NotFoundError); ok != true {
			ctx.AbortWithStatus(http.StatusNotFound)
			return
		}
	}

	ctx.JSON(http.StatusOK, gin.H{})
}

func HandleGetUsers(ctx *gin.Context) {

	witcherID := ctx.Query("witcher_id")
	role := ctx.Query("role")
	isDeadParam := ctx.Query("is_dead")
	isBannedParam := ctx.Query("is_banned")
		
	users, err := services.GetUsers(witcherID, role, isDeadParam, isBannedParam)
	if err != nil{
		if _, ok := err.(*exceptions.StoreError); ok {
			ctx.AbortWithStatus(http.StatusInternalServerError)
			return
		}
		if _, ok := err.(*exceptions.ParseError); ok != true {
			ctx.AbortWithStatus(http.StatusNotFound)
			return
		}
	}

	ctx.JSON(http.StatusOK, users)
}

func HandleUpdateUser(ctx *gin.Context) {

	var body helpers.InputUserUpdateSchema

	if ctx.Bind(&body) != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Failed to read body",
		})
		return
	}

	id := ctx.Param("id")
	err := services.UpdateUser(id, &body)
	if err != nil{
		if _, ok := err.(*exceptions.StoreError); ok {
			ctx.AbortWithStatus(http.StatusInternalServerError)
			return
		}
	}
	ctx.JSON(http.StatusOK, gin.H{})
}
