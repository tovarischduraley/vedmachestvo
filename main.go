package main

import (
	"vedmachestvo/controllers"
	"vedmachestvo/initializers"
	"vedmachestvo/middleware"

	"github.com/gin-gonic/gin"
)

func init() {
	initializers.LoadEnvVariables()
	initializers.ConnectToDB()
}

func main() {
	r := gin.Default()
	r.Use(middleware.CORSMiddleware())
	r.GET("/quests", controllers.HandleGetQuests)
	r.POST("/quests", middleware.RequireAuth, middleware.RequireRoleTeacher, controllers.HandleCreateQuest)
	r.GET("/quests/:id", controllers.HandleGetQuestById)
	r.PATCH("/quests/:id", middleware.RequireAuth, controllers.HandleUpdateQuest)
	r.DELETE("/quests/:id", middleware.RequireAuth, controllers.HandleDeleteQuest)
	r.GET("/quests/:id/check", controllers.HandleCheckQuestIsReady)
	r.GET("/users", controllers.HandleGetUsers)
	r.POST("/users", middleware.RequireAuth, controllers.HandleCreateUser)
	r.GET("/users/:id", controllers.HandleGetUserById)
	r.PATCH("/users/:id", controllers.HandleUpdateUser)
	r.POST("/users/:id/kill", controllers.HandleKillUser)
	r.POST("/users/:id/upgrade", controllers.HandleUpgradeStudent)
	r.POST("/users/:id/ban", controllers.HandleBanUser)
	r.POST("/users/:id/unban", controllers.HandleUnbanUser)
	r.GET("/laws", middleware.RequireAuth, middleware.RequireRoleTeacher, controllers.HandleGetLaws)
	r.POST("/laws", controllers.HandleCreateLaw)
	r.GET("/laws/:id", middleware.RequireAuth, middleware.RequireRoleTeacher, controllers.HandleGetLawById)
	r.PATCH("/laws/:id", middleware.RequireAuth, middleware.RequireRoleTeacher, controllers.HandleUpdateLaw)
	r.DELETE("/laws/:id", middleware.RequireAuth, middleware.RequireRoleTeacher, controllers.HandleDeleteLaw)
	r.POST("/laws/:id/upgrade", middleware.RequireAuth, middleware.RequireRoleTeacher, controllers.HandleLawUpgrade)
	r.POST("/login", controllers.HandleLogin)
	r.POST("/logout", controllers.HandleLogout)
	r.GET("/me", middleware.RequireAuth, controllers.HandleValidate)
	r.Run(":8082")
}
