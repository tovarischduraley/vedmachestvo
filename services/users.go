package services

import (
	"strconv"
	"vedmachestvo/exceptions"
	"vedmachestvo/helpers"
	"vedmachestvo/initializers"
	"vedmachestvo/models"

	"golang.org/x/crypto/bcrypt"
)

func GetUserById(id string) (*models.User, error) {
	var user models.User
	ret := initializers.DB.Preload("Witcher").First(&user, "id = ?", id)

	if ret.RowsAffected == 0 {
		return nil, &exceptions.NotFoundError{Message: "Law id = " + id + " not found"}
	}

	if ret.Error != nil {
		return nil, &exceptions.StoreError{Message: ret.Error.Error()}
	}
	return &user, nil
}

func CreateUser(body *helpers.InputUserCreateSchema) error{

	hash, err := bcrypt.GenerateFromPassword([]byte(body.Password), 10)
	if err != nil {
		return &exceptions.ValidationError{Message: "Failed to hash password"}
	}

	info := &helpers.ProfileInfo{
		Role:         body.Role,
		Level:        body.Level,
		Agility:      body.Agility,
		Intelligence: body.Intelligence,
		Strength:     body.Strength,
	}
	profile, err := helpers.ValidateStats(info)

	if err != nil {
		return &exceptions.ValidationError{Message: err.Error()}
	}

	var witcherID *uint
	switch profile.Role {
	case helpers.Death, helpers.Witcher, helpers.Headmaster:
		witcherID = nil
	case helpers.Student:
		witcherID = body.WitcherID
	}

	user := models.User{
		Email:        body.Email,
		Password:     string(hash),
		Name:         body.Name,
		Role:         profile.Role,
		Level:        profile.Level,
		Agility:      profile.Agility,
		Intelligence: profile.Intelligence,
		Strength:     profile.Strength,
		WitcherID:    witcherID,
		IsDead:       false,
		IsBanned:     false,
	}

	result := initializers.DB.Create(&user)

	if result.Error != nil {
		return &exceptions.StoreError{Message: result.Error.Error()}
	}
	return nil
}

func KillUser(id string) error{
	ret := initializers.DB.Model(&models.Quest{}).Where("student_id = ?", id).Updates(map[string]interface{}{"student_id": nil, "status": models.New})

	if ret.Error != nil {
		return &exceptions.StoreError{Message: ret.Error.Error()}
	}

	ret = initializers.DB.Model(&models.User{}).Where("id = ?", id).Updates(map[string]interface{}{"is_dead": true})

	if ret.Error != nil {
		return &exceptions.StoreError{Message: ret.Error.Error()}
	}
	return nil
}

func UpgrageStudent(id string) error{
	ret := initializers.DB.First(&models.User{}, "id = ? and role = ?", id, helpers.Student)
	if ret.RowsAffected == 0 {
		return &exceptions.NotFoundError{Message: "Student not found"}
	}

	ret = initializers.DB.Model(&models.Quest{}).Where("student_id = ?", id).Updates(map[string]interface{}{"student_id": nil, "status": models.New})

	if ret.Error != nil {
		return &exceptions.StoreError{Message: ret.Error.Error()}
	}

	ret = initializers.DB.Model(&models.User{}).Where("id = ?", id).Updates(map[string]interface{}{"role": helpers.Witcher, "witcher_id": nil})

	if ret.Error != nil {
		return &exceptions.StoreError{Message: ret.Error.Error()}
	}
	return nil
}

func UnbanUser(id string) error {
	ret := initializers.DB.First(&models.User{}, "id = ? and (role = ? or role =?) and is_banned = true", id, helpers.Student, helpers.Witcher)
	if ret.RowsAffected == 0 {
		return &exceptions.NotFoundError{Message: "Student not found"}
	}
	ret = initializers.DB.Model(&models.User{}).Where("id = ?", id).Updates(map[string]interface{}{"is_banned": false})

	if ret.Error != nil {
		return &exceptions.StoreError{Message: ret.Error.Error()}
	}
	return nil
}

func BanUser(id string) error {
	ret := initializers.DB.First(&models.User{}, "id = ? and (role = ? or role =?)", id, helpers.Student, helpers.Witcher)
	if ret.RowsAffected == 0 {
		return &exceptions.NotFoundError{Message: "Student not found"}

	}

	ret = initializers.DB.Model(&models.Quest{}).Where("student_id = ?", id).Updates(map[string]interface{}{"student_id": nil, "status": models.New})
	if ret.Error != nil {
		return &exceptions.StoreError{Message: ret.Error.Error()}
	}

	ret = initializers.DB.Model(&models.User{}).Where("witcher_id = ?", id).Updates(map[string]interface{}{"witcher_id": nil})
	if ret.Error != nil {
		return &exceptions.StoreError{Message: ret.Error.Error()}
	}

	ret = initializers.DB.Model(&models.User{}).Where("id = ?", id).Updates(map[string]interface{}{"is_banned": true})

	if ret.Error != nil {
		return &exceptions.StoreError{Message: ret.Error.Error()}
	}
	return nil
}

func GetUsers(witcherID string, role string, isDeadParam string, isBannedParam string) ([]models.User, error){
	var users []models.User
	base := initializers.DB
	if witcherID != "" {
		if witcherID == "null" {
			base = base.Where("witcher_id IS NULL")
		} else {
			WID, err := strconv.Atoi(witcherID)
			if err != nil {
				return nil, &exceptions.ParseError{Message: err.Error()}
			}
			uWID := uint(WID)
			base = base.Where(&models.User{WitcherID: &uWID})
		}
	}

	if role != "" {
		base = base.Where(&models.User{Role: role})
	}

	if isDeadParam != "" {
		isDead, _ := strconv.ParseBool(isDeadParam)
		base = base.Where(&models.User{IsDead: isDead})
	}

	if isBannedParam != "" {
		isBanned, _ := strconv.ParseBool(isBannedParam)
		base = base.Where(&models.User{IsBanned: isBanned})
	}

	ret := base.Preload("Witcher").Find(&users)
	if ret.Error != nil {
		return nil, &exceptions.StoreError{Message: ret.Error.Error()}
	}
	return users, nil
}

func UpdateUser(id string, body *helpers.InputUserUpdateSchema) error {
	ret := initializers.DB.Model(&models.User{}).Where("id = ?", id).Updates(body)

	if ret.Error != nil {
		return &exceptions.StoreError{Message: ret.Error.Error()}
	}
	return nil
}

