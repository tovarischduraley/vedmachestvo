package services

import (
	"strconv"
	"vedmachestvo/exceptions"
	"vedmachestvo/helpers"
	"vedmachestvo/initializers"
	"vedmachestvo/models"

	"golang.org/x/crypto/bcrypt"
)

func GetLaws(witcherID string) ([]models.Law, error) {

	var laws []models.Law
	base := initializers.DB
	if witcherID != "" {
		base = base.Where("witcher_id = ?", witcherID)
	}

	ret := base.Preload("Witcher").Find(&laws)

	if ret.Error != nil {
		return nil, &exceptions.StoreError{Message: ret.Error.Error()}
	}
	return laws, nil
}

func GetLawById(id string) (*models.Law, error) {
	var law models.Law
	ret := initializers.DB.Preload("Witcher").First(&law, "id = ?", id)

	if ret.Error != nil {
		return nil, &exceptions.StoreError{Message: ret.Error.Error()}
	}

	if ret.RowsAffected == 0 {
		return nil, &exceptions.NotFoundError{Message: "Law id = " + id + " not found"}
	}

	return &law, nil
}

func CreateLaw(body *helpers.InputLawSchema) error {

	if body.WitcherID == 0 {
		return &exceptions.StoreError{Message: "Witcher must be passed"}
	}

	witcher, _ := GetUserById(strconv.Itoa(int(body.WitcherID)))
	ok := helpers.ValidateRole(witcher, helpers.Witcher)
	if !ok {
		return &exceptions.ValidationError{Message: "Law should belong to 'Witcher'"}
	}

	law := models.Law{
		Name:        body.Name,
		Description: body.Description,
		Location:    body.Location,
		WitcherID:   body.WitcherID,
	}

	result := initializers.DB.Create(&law)

	if result.Error != nil {
		return &exceptions.StoreError{Message: result.Error.Error()}
	}
	return nil
}

func UpdateLaw(id string, body *helpers.InputLawUpdateSchema) error {
	ret := initializers.DB.Model(&models.Law{}).Where("id = ?", id).Updates(body)

	if ret.Error != nil {
		return &exceptions.StoreError{Message: ret.Error.Error()}
	}
	return nil
}

func DeleteLaw(id string) error {
	ret := initializers.DB.Delete(&models.Law{}, id)
	if ret.Error != nil {
		return &exceptions.StoreError{Message: ret.Error.Error()}
	}
	return nil
}

func UpgrageLaw(id string, body *helpers.InputLawUpgradeSchema) error {
	var law models.Law
	ret := initializers.DB.First(&law, "id = ?", id)

	if ret.Error != nil {
		return &exceptions.NotFoundError{Message: "Law id = " + id + " not found"}
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(body.Password), 10)
	if err != nil {
		return &exceptions.ValidationError{Message: "Failed to hash password"}
	}

	user := models.User{
		Email:        body.Email,
		Password:     string(hash),
		Strength:     body.Strength,
		Agility:      body.Agility,
		Intelligence: body.Intelligence,

		Name:        law.Name,
		Description: law.Description,
		Level:       1,
		Role:        helpers.Student,
		IsDead:      false,
		IsBanned:    false,
		WitcherID:   &law.WitcherID,
	}

	ret = initializers.DB.Create(&user)
	if ret.Error != nil {
		return &exceptions.StoreError{Message: "Failed to create user"}
	}

	ret = initializers.DB.Delete(&law)
	if ret.Error != nil {
		return &exceptions.StoreError{Message: "Failed to delete law"}
	}

	return nil
}
