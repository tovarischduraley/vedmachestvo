package services

import (
	"time"
	"vedmachestvo/exceptions"
	"vedmachestvo/helpers"
	"vedmachestvo/initializers"
	"vedmachestvo/models"
)

func GetQuestById(id string) (*models.Quest, error) {
	var quest models.Quest
	ret := initializers.DB.Preload("Witcher").Preload("Student").First(&quest, "id = ?", id)

	if ret.RowsAffected == 0 {
		return nil, &exceptions.NotFoundError{Message: "Quest not found"}
	}

	if ret.Error != nil {
		return nil, &exceptions.StoreError{Message: ret.Error.Error()}
	}
	return &quest, nil
}

func DeleteQuest(id string) error {
	ret := initializers.DB.Delete(&models.Quest{}, id)
	if ret.Error != nil {
		return &exceptions.StoreError{Message: ret.Error.Error()}
	}
	return nil
}

func CheckQuestIsReady(id string) (*helpers.QuestIsReadyDTO, error) {
	var quest models.Quest

	ret := initializers.DB.First(&quest, "id = ?", id)

	if ret.RowsAffected == 0 {
		return nil, &exceptions.NotFoundError{Message: "Quest not found"}
	}

	if ret.Error != nil {
		return nil, &exceptions.StoreError{Message: ret.Error.Error()}
	}
	var response helpers.QuestIsReadyDTO

	response.IsDone = quest.IsDone
	time.Sleep(5 * time.Second)
	return &response, nil
}