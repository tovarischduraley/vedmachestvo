package main

import (
	"fmt"
	"log"
	"vedmachestvo/helpers"
	"vedmachestvo/initializers"
	"vedmachestvo/models"

	"golang.org/x/crypto/bcrypt"
)

func init() {
	initializers.LoadEnvVariables()
	initializers.ConnectToDB()
}

func main() {
	hash, err := bcrypt.GenerateFromPassword([]byte("123"), 10)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Hash is %s\n", string(hash))
	head := models.User{
		Name:     "Весимир",
		Email:    "head@head.head",
		Password: string(hash),
		Role:     helpers.Headmaster,
	}

	witcher := models.User{
		Name:     "Геральт",
		Email:    "witcher@witcher.witcher",
		Password: string(hash),
		Role:     helpers.Witcher,
	}
	initializers.DB.Create(&head)
	initializers.DB.Create(&witcher)
	var created models.User
	initializers.DB.First(&created, "role = ?", helpers.Witcher)

	student := models.User{
		Name:     "Цири",
		Email:    "student@student.student",
		Password: string(hash),
		Role:     helpers.Student,
		WitcherID: &created.ID,
	}

	death := models.User{
		Name:     "На бледном коне",
		Email:    "death@death.death",
		Password: string(hash),
		Role:     helpers.Death,
	}

	initializers.DB.Create(&death)
	initializers.DB.Create(&student)

	fmt.Println(head)
	fmt.Println(witcher)
	fmt.Println(student)
	fmt.Println(death)

}
