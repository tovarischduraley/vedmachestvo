package tests

import (
	"strconv"
	"testing"
	"vedmachestvo/initializers"
	"vedmachestvo/models"
	"vedmachestvo/services"

	"github.com/stretchr/testify/assert"
)

func init() {
	initializers.LoadEnvVariables()
	initializers.ConnectToDB()
}

func TestQuetsCheckIsReady(t *testing.T) {
	var last_quest models.Quest
	initializers.DB.Last(&last_quest)

	last_id := strconv.Itoa(int(last_quest.ID))

	resp, err := services.CheckQuestIsReady(last_id)

	assert.NoError(t, err)
	assert.Equal(t, &resp.IsDone, &last_quest.IsDone)
}