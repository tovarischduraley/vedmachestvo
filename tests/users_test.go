package tests

import (
	"strconv"
	"testing"
	"vedmachestvo/helpers"
	"vedmachestvo/initializers"
	"vedmachestvo/models"
	"vedmachestvo/services"

	"github.com/stretchr/testify/assert"
)

func init(){
	initializers.LoadEnvVariables()
	initializers.ConnectToDB()
}

func TestUserGetById(t *testing.T) {
	law, err := services.GetUserById("1")
	assert.Equal(t, err, nil)
	assert.Equal(t, law.ID, uint(1))
}

func TestUserCreate(t *testing.T){
	body := helpers.InputUserCreateSchema{
		Email: "test",
		Password: "123",
		Name: "test",
		Role: "student",
		Level: 1,
		Agility: 1,
		Intelligence: 1,
		Strength: 1,
		WitcherID: nil,
	}

	var users_count_before int64
	var users_count_after int64

	initializers.DB.Model(&models.User{}).Count(&users_count_before)
	
	assert.NoError(t, services.CreateUser(&body))

	initializers.DB.Model(&models.User{}).Count(&users_count_after)

	assert.Equal(t, users_count_before, users_count_after - 1)
}


func TestUserUpgrade(t *testing.T){

	var last_student models.User
	initializers.DB.Last(&last_student, "role = ?", helpers.Student)
	last_id := strconv.Itoa(int(last_student.ID))

	var witchers_count_before int64
	var witchers_count_after int64
	var students_count_before int64
	var students_count_after int64

	initializers.DB.Model(&models.User{}).Where("role = ?", helpers.Student).Count(&students_count_before)
	initializers.DB.Model(&models.User{}).Where("role = ?", helpers.Witcher).Count(&witchers_count_before)
	
	assert.NoError(t, services.UpgrageStudent(last_id))

	initializers.DB.Model(&models.User{}).Where("role = ?", helpers.Student).Count(&students_count_after)
	initializers.DB.Model(&models.User{}).Where("role = ?", helpers.Witcher).Count(&witchers_count_after)

	assert.Equal(t, students_count_before, students_count_after + 1)
	assert.Equal(t, witchers_count_before, witchers_count_after - 1)
}