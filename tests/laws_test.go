package tests

import (
	"strconv"
	"testing"
	"vedmachestvo/helpers"
	"vedmachestvo/initializers"
	"vedmachestvo/models"
	"vedmachestvo/services"

	"github.com/stretchr/testify/assert"
)

func init(){
	initializers.LoadEnvVariables()
	initializers.ConnectToDB()
}

func TestLawCreate(t *testing.T){
	var witcher models.User

	db := initializers.DB
	ret := db.First(&witcher, "role = ?", helpers.Witcher)

	assert.NotEqual(t, ret.RowsAffected, 0)
	
	law := helpers.InputLawSchema{
		Name: "test_name",
		Location: "test_location",
		Description: "test_description",
		WitcherID: witcher.ID,
	}
	var last_law models.Law
	ret = db.Last(&last_law)

	assert.NotEqual(t, ret.RowsAffected, 0)
	assert.NoError(t, services.CreateLaw(&law))

	var new_law models.Law
	ret = db.Last(&new_law)
	assert.NotEqual(t, last_law.ID, new_law.ID)
}

func TestLawGetById(t *testing.T){
	law, err := services.GetLawById("1")
	assert.Equal(t, err, nil)
	assert.Equal(t, law.ID, uint(1))
}

func TestLawUpgrade(t *testing.T){
	create_body := helpers.InputLawSchema{
		Name: "test_name",
		Location: "test_location",
		Description: "test_description",
		WitcherID: 1,
	}
	
	assert.NoError(t, services.CreateLaw(&create_body))

	var last_law models.Law
	initializers.DB.Last(&last_law)
	last_id := strconv.Itoa(int(last_law.ID))


	body := helpers.InputLawUpgradeSchema{
		Agility: 1,
		Intelligence: 1,
		Strength: 1,
		Email: "test",
		Password: "123",
	}

	var users_count_before int64
	var laws_count_before int64
	var users_count_after int64
	var laws_count_after int64 

	initializers.DB.Model(&models.User{}).Count(&users_count_before)
	initializers.DB.Model(&models.Law{}).Count(&laws_count_before)
	
	assert.NoError(t, services.UpgrageLaw(last_id, &body))

	initializers.DB.Model(&models.User{}).Count(&users_count_after)
	initializers.DB.Model(&models.Law{}).Count(&laws_count_after)

	assert.Equal(t, users_count_before, users_count_after - 1)
	assert.Equal(t, laws_count_before, laws_count_after + 1)
}
