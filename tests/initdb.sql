
CREATE TABLE users (
   id bigserial not null primary key,
   created_at timestamp,
   updated_at timestamp,
   deleted_at timestamp,
   email varchar,
   password varchar,
   name varchar,
   description varchar,
   role varchar,
   level integer,
   agility integer,
   intelligence integer,
   strength integer,
   is_dead boolean,
   is_banned boolean,
   witcher_id integer references users (id)
);

CREATE TABLE laws (
   id bigserial not null primary key,
   created_at timestamp,
   updated_at timestamp,
   deleted_at timestamp,
   name varchar,
   description varchar,
   location varchar,
   witcher_id integer references users (id)
);

CREATE TABLE quests (
   id bigserial not null primary key,
   created_at timestamp,
   updated_at timestamp,
   deleted_at timestamp,
   title varchar,
   status varchar,
   description varchar,
   location integer,
   preferred_level integer,
   is_done boolean,
   most_needed_attribute varchar,
   witcher_id integer references users (id),
   student_id integer references users (id)
);

INSERT INTO users(name, description, email, password, role, witcher_id) values ('w', 'test', 'testw@testw.test', '$2a$10$IMTI2CYQymEL6Z31t6graO6J34Tbp02GQdPGVodSbWZpQR3ukimJ.', 'witcher', null);
INSERT INTO users(name, description, email, password, role, witcher_id) values ('s', 'test', 'tests@test.test', '$2a$10$IMTI2CYQymEL6Z31t6graO6J34Tbp02GQdPGVodSbWZpQR3ukimJ.', 'student', 1);
INSERT INTO laws(name, description, location, witcher_id) values ('test', 'test', 'test', 1);
INSERT INTO quests(title, description, witcher_id, student_id) values ('test', 'test', 1, 2)

