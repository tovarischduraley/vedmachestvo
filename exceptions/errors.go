package exceptions

type ParseError struct {
	Message string
}

func (e *ParseError) Error() string {
	return e.Message
}

type NotFoundError struct {
	Message string
}

func (e *NotFoundError) Error() string {
	return e.Message
}

type StoreError struct {
	Message string
}

func (e *StoreError) Error() string {
	return e.Message
}

type ValidationError struct {
	Message string
}

func (e *ValidationError) Error() string {
	return e.Message
}
