package helpers

type InputQuestCreateSchema struct {
	Title               string `json:"title"`
	Description         string `json:"description"`
	PreferredLevel      int    `json:"preferred_level"`
	MostNeededAttribute string `json:"most_needed_attribute"`
	StudentID           uint   `json:"student_id,omitempty"`
}

type QuestIsReadyDTO struct {
	IsDone bool `json:"is_done"`
}
