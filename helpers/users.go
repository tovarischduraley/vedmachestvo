package helpers

import (
	"vedmachestvo/exceptions"
	"vedmachestvo/models"
)

func ValidateRole(user *models.User, role string) bool {
	return user.Role == role
}

type InputUserCreateSchema struct {
	Email        string `json:"email"`
	Password     string `json:"password"`
	Name         string `json:"name"`
	Role         string `json:"role"`
	Level        int    `json:"level,omitempty"`
	Agility      int    `json:"agility,omitempty"`
	Intelligence int    `json:"intelligence,omitempty"`
	Strength     int    `json:"strength,omitempty"`
	WitcherID    *uint   `json:"witcher_id,omitempty"`
}

type InputUserUpdateSchema struct {
	Email        string `json:"email,omitempty"`
	Password     string `json:"password,omitempty"`
	Name         string `json:"name,omitempty"`
	Description  string `json:"description,omitempty"`
	Role         string `json:"role,omitempty"`
	Level        int    `json:"level,omitempty"`
	Agility      int    `json:"agility,omitempty"`
	Intelligence int    `json:"intelligence,omitempty"`
	Strength     int    `json:"strength,omitempty"`
	IsDead       bool   `json:"is_dead,omitempty"`
	IsBanned     bool   `json:"is_banned,omitempty"`
	WitcherID    int    `json:"witcher_id,omitempty"`
}

type ProfileInfo struct {
	Role         string
	Level        int
	Agility      int
	Intelligence int
	Strength     int
}

func ValidateStats(info *ProfileInfo) (*ProfileInfo, error) {
	var profile ProfileInfo

	if info.Role == "" {
		profile.Role = Student
	} else {
		profile.Role = info.Role
	}
	switch profile.Role {
	case Death:
		profile.Level = -1
		profile.Agility = -1
		profile.Intelligence = -1
		profile.Strength = -1
	case Witcher, Headmaster:
		profile.Level = 100
		profile.Agility = 100
		profile.Intelligence = 100
		profile.Strength = 100
	case Student:
		if info.Level > 0 {
			profile.Level = info.Level
		} else {
			profile.Level = 1
		}

		if info.Agility > 0 {
			profile.Agility = info.Agility
		} else {
			profile.Agility = 1
		}

		if info.Intelligence > 0 {
			profile.Intelligence = info.Intelligence
		} else {
			profile.Intelligence = 1
		}

		if info.Strength > 0 {
			profile.Strength = info.Strength
		} else {
			profile.Strength = 1
		}
	default:
		return nil, &exceptions.ValidationError{Message: "role is not valid"}
	}
	return &profile, nil
}

const (
	Witcher    = "witcher"
	Student    = "student"
	Death      = "death"
	Headmaster = "headmaster"
)
