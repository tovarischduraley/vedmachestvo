package helpers

type InputLawSchema struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Location    string `json:"location"`
	WitcherID   uint   `json:"witcher_id"`
}

type InputLawUpdateSchema struct {
	Name        string `json:"name,omitempty"`
	Description string `json:"description,omitempty"`
	Location    string `json:"location,omitempty"`
}

type InputLawUpgradeSchema struct {
	Agility      int    `json:"agility,omitempty"`
	Intelligence int    `json:"intelligence,omitempty"`
	Strength     int    `json:"strength,omitempty"`
	Email        string `json:"email,omitempty"`
	Password     string `json:"password,omitempty"`
}
